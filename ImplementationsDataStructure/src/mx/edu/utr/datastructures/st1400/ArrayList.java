/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructures.st1400;
import mx.edu.utr.datastructures.*;
import java.util.Arrays;
/**
 *
 * @author Jorge Adrian Serna Jimenez
 */

public class ArrayList implements List {

    
    private Object[] elements;
    private int size;
    public ArrayList(){
    this(10);
    }
    
    public ArrayList(int initialCapacity){
        elements = new Object [initialCapacity];
    }
    
    /**
     * {@inheritDoc      * 
     * @param element
     * @return 
     */
    @Override
    public boolean add(Object element) {
       ensureCapacity (size+1);
       elements[size++]= element;
       return true;
    }
/**
 * {@inheritDoc 
 * @param index
 * @param element 
 */
    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        ensureCapacity(size+1);
        elements[index]=element;
        size++;
        
    }
/**
 * {@inheritDoc 
 */
    @Override
    public void clear() {
        for(int i=0; i<size; i++){
            elements[i]=null;
            size=0;
        }
    }
/**
 * {@inheritDoc 
 * @param index
 * @return 
 */
    @Override
    public Object get(int index) {
        outOfBound(index);
        return elements[index];
    }
/**
 * {@inheritDoc 
 * @param element
 * @return 
 */
    @Override
    public int indexOf(Object element) {
       if(element == null){
            for (int i = 0; i < size; i++) {
                if (elements.equals(elements[1])) {
                    return i;
                }
                
            }
            
        }
        else
        {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
                
            }
        }
       return -1;
    }
/**
 * {@inheritDoc 
 * @return 
 */
    @Override
    public boolean isEmpty() {
        return size==0;
    }
/**
 * {@inheritDoc 
 * @param index
 * @return 
 */
    @Override
    public Object remove(int index) {
       
       outOfBound(index);
       Object oldElement = elements[index];
       int numberMoved = size - index - 1;
       if(numberMoved>0){
           System.arraycopy(elements, index + 1, elements , index, numberMoved);
       }
       elements[--size]= null;
       return oldElement;
    }
/**
 * {@inheritDoc 
 * @param index
 * @param element
 * @return 
 */
    @Override
    public Object set(int index, Object element) {
        outOfBound(index);
        Object old=elements[index];
        elements[index]=element;
        return old;
    }
/**
 * {@inheritDoc 
 * @return 
 */
    @Override
    public int size() {
        return size;
    }
/**
 * {@inheritDoc 
 * @param minCapacity 
 */
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if(minCapacity >oldCapacity){
            int newCapacity = oldCapacity*2;
            if(newCapacity<oldCapacity){
                newCapacity=minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }
/**
 * {@inheritDoc 
 * @param index 
 */
    private void rangeCheckForAdd(int index) {
        if(index>size-1 || index<0){
            throw new IndexOutOfBoundsException();
        }
    }
/**
 * {@inheritDoc 
 * @param index 
 */
    private void outOfBound(int index) {
        if (index>=size){
            throw new ArrayIndexOutOfBoundsException("Out of Bound");
        }
    }
    
    
    
}
